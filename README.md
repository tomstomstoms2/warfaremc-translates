# WARFAREMC TRANSLATES #

[![](https://img.shields.io/badge/P%C5%99ipojit%20se-Discord-blue)](https://discord.gg/C3Vn4aY)

## BeehivePro
* Přeloženo - sad_mirai

## CMI
* Přeloženo - sad_mirai, Alu01

## CrazyAuctions
* Překládá se - sad_mirai

## DeathMessagesPrime
* Přeloženo - sad_mirai

## DeluxeTags
* Přeloženo - sad_mirai

## FastAsyncWorldEdit
* Překládá se - sad_mirai

## Jobs Reborn
* Překládá se, upravuje se - Original Author & sad_mirai

## LockettePro
* Přeloženo - sad_mirai

## MiniaturePets
* Přeloženo - sad_mirai

## PlayerWarps
* Přeloženo - sad_mirai

## Quests
* Přeloženo - sad_mirai

## QuickShop
* Přeloženy hlavní fráze - sad_mirai

## Residence
* Doupravují se chybné překlady, přidává se diakritika, překládají se nepřeložené fráze - sad_mirai

## ShopGuiPlus
* Přeloženo - Alu01, sad_mirai

## TradeMe
* Přeloženy jen podstatné věci, překládá se - sad_mirai

## mcMMO
* Přeloženo - Alu01

